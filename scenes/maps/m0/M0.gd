extends Node2D

func _ready():
	for vec in $Trees.get_used_cells():
		var cell = $Trees.get_cell(vec.x,vec.y)
		print(vec,' ',cell,' ',$Trees.get_cellv(vec))
		makeTile(vec,cell)
	$Trees.hide()
	$Trees.queue_free()

func _input(event):
	if event.is_action_pressed("click"):
		print(event.global_position)


func makeTile(pos:Vector2,cell:int):
	var tileMap = TileMap.new()
	$TreeHolder.add_child(tileMap)
	tileMap.cell_size = $Trees.cell_size
	tileMap.tile_set = $Trees.tile_set
	tileMap.global_position = $Trees.map_to_world(pos)
	#tileMap.set_cell(pos.x,pos.y,cell)
	tileMap.set_cell(0,0,cell)
	#tileMap.z_index = tileMap.map_to_world(pos).y+getTileMapCellSizeAtTilePos($Trees,pos).y
	tileMap.z_index = tileMap.global_position.y+getTileMapCellSizeAtTilePos($Trees,pos).y
	var tileName = tileMap.tile_set.tile_get_name(cell)
	if "door" in tileName:
		tileMap.add_to_group("doors")
	print(tileMap.z_index)


func getGroundCellNameAt(pos:Vector2)->String:
	return getTileMapCellNameAt($Ground,pos)

static func getTileMapCellNameAt(tilemap:TileMap,pos:Vector2)->String:
	var local_position = tilemap.to_local(pos)
	var tile_position = tilemap.world_to_map(local_position)
	var tile_id = tilemap.get_cellv(tile_position)
	var tile_name = tilemap.tile_set.tile_get_name(tile_id)
	return tile_name


static func getTileMapCellSizeAt(tilemap:TileMap,pos:Vector2)->Vector2:
	var local_position = tilemap.to_local(pos)
	var tile_position = tilemap.world_to_map(local_position)
	return getTileMapCellSizeAtTilePos(tilemap,tile_position)


static func getTileMapCellSizeAtTilePos(tilemap:TileMap,tile_position:Vector2)->Vector2:
	var tile_id = tilemap.get_cellv(tile_position)
	var tile_width = tilemap.tile_set.tile_get_texture(tile_id).get_width()
	var tile_height = tilemap.tile_set.tile_get_texture(tile_id).get_height()
	return Vector2(tile_width,tile_height)
