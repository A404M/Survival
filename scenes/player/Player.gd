extends KinematicBody2D

export var acceleration := 800.0
export var moveSpeed := 150.0
export var runSpeed := 250.0
export var runningTime := 6.0
export var recoveryTimeStand := 10.0
export var recoveryTimeWalk := 20.0
export var minimumRecoveryTime := 3.0

var velocity := Vector2.ZERO

var isSlipping := false
var isRunning := false
var isMoving := false

var runningTimeProgress := runningTime

func _input(event):
	if event.is_action_pressed("run"):
		if runningTimeProgress >= minimumRecoveryTime:
			isRunning = true
	if event.is_action_released("run"):
		isRunning = false

func _process(delta):
	$Control/FPS.text = "FPS: %d"%Engine.get_frames_per_second()
	
	#print(get_tree().get_nodes_in_group("doors"))
	var minDistance:float = 1000
	var minNode:TileMap
	var minPosition:Vector2
	for door in get_tree().get_nodes_in_group("doors"):
		var distance = global_position.distance_to(door.global_position)
		if distance < minDistance:
			minNode = door
			minPosition = door.global_position
			minDistance = distance
	
	if minDistance < 100:
		minPosition.y -= 20
		$ActionHint.show()
		$ActionHint.set_global_position(minPosition)
		if Input.is_action_just_pressed("open"):
			var doorName = minNode.tile_set.tile_get_name(
					minNode.get_cell(0,0)
				)
			doorName = doorName.replace("open","closed") if "open" in doorName else doorName.replace("closed","open")
			minNode.set_cell(0,0,
				minNode.tile_set.find_tile_by_name(
						doorName
					)
				)
	else:
		$ActionHint.hide()

func _physics_process(delta):
	var direction = getMoveDirection()
	
	if isSlipping:
		if direction != Vector2.ZERO:
			#velocity += direction
			velocity = velocity.linear_interpolate(direction*getMoveSpeed(),0.05)
			velocity = velocity.limit_length(getMoveSpeed())
			#velocity.normalized()
		else:
			velocity *= 0.98
	else:
		velocity = direction*getMoveSpeed()
	
	velocity = move_and_slide(velocity,Vector2.UP)
	
	z_index = position.y
	if Input.is_key_pressed(KEY_Z):
		print(z_index)


func getMoveDirection() -> Vector2:
	var direction := Vector2.ZERO
	
	direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	direction.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	#print(direction)
	isMoving = direction.length() != 0
	return direction.normalized()


func getMoveSpeed()->float:
	if runningTimeProgress == 0:
		Input.action_release("run")
		return moveSpeed
	return runSpeed if isRunning else moveSpeed

func _on_Timer_timeout():
	if isRunning:
		runningTimeProgress -= $Timer.wait_time
	else:
		runningTimeProgress += $Timer.wait_time*(runningTime/getRecoveryTime())
	if runningTimeProgress < 0:
		runningTimeProgress = 0
	elif runningTimeProgress > runningTime:
		runningTimeProgress = runningTime
	$Control/Status/StaminaBar.value = (runningTimeProgress/runningTime)*100.0


func getRecoveryTime()->float:
	return recoveryTimeWalk if isMoving else recoveryTimeStand
