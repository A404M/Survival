class_name Main

extends Node2D

func _process(delta):
	pass

func _physics_process(delta):
	$Player.isSlipping = "slippy" in $SceneHolder/Map.getGroundCellNameAt($Player.global_position)


func _input(event):
	if event.is_action_pressed("full_screen"):
		OS.window_fullscreen = not OS.window_fullscreen
