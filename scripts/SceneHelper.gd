class_name SceneHelper


static func changeScene(current:Node,next:Node):
	var parent := current.get_parent()
	next.name = current.name
	parent.remove_child(current)
	current.queue_free()
	parent.add_child(next)
	next.owner = parent


static func freeChildren(node:Node):
	for child in node.get_children():
		node.remove_child(child)
		child.queue_free()
